local sensorInfo = {
	name = "GetPoints",
	desc = "",
	author = "bentosming",
	date = "2019-04-23",
	license = "MIT"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching  -- !!!

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description gets positions along east, west and south edges of map

return function()

		local mi =  Sensors.core.MissionInfo()
		local dangerPoint
		local dangerPointarg = 0
		for i, point in ipairs(mi.corridors['Middle'].points) do
			if (point.isStrongpoint and point.ownerAllyID == 1 ) then
				dangerPoint = point.position
				dangerPointarg = i
				break
			end
		end
		if dangerPointarg==0 then
			return {p = mi.corridors['Middle'].points[#mi.corridors['Middle'].points].position,
				safePoint = mi.corridors['Middle'].points[#mi.corridors['Middle'].points].position,
				dangerPoint = mi.corridors['Middle'].points[#mi.corridors['Middle'].points].position
			}
		end

		local safePoint = mi.corridors['Middle'].points[dangerPointarg-4].position
		local p = safePoint
		for t = 0.01, 1, 0.03 do
			xx = (safePoint.x - dangerPoint.x) * t + dangerPoint.x
			zz = (safePoint.z - dangerPoint.z) * t + dangerPoint.z
			yy = (safePoint.y - dangerPoint.y) * t + dangerPoint.y
			pp = Vec3(xx,yy,zz)
			if pp:Distance(dangerPoint)>1100 and pp:Distance(dangerPoint)<1250 then
				p = pp
				break
			end
			
		end


	return {p=p, safePoint = safePoint, dangerPoint = dangerPoint }
end
