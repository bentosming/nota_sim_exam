local sensorInfo = {
	name = "GetPoints",
	desc = "",
	author = "bentosming",
	date = "2019-04-23",
	license = "MIT"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching  -- !!!

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end



return function(pathId)
	if pathId == 1 then return "Top" end
	if pathId == 2 then return "Middle" end
	if pathId == 3 then return "Bottom" end
end
