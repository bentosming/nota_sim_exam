local sensorInfo = {
	name = "GetPoints",
	desc = "",
	author = "bentosming",
	date = "2019-04-23",
	license = "MIT"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local GetGroundHeight = Spring.GetGroundHeight
local GetTeamUnits = Spring.GetTeamUnits

-- @description 

return function()
	local metal = Spring.GetTeamResources(0, 'metal')
	
	if #Sensors.nota_sim_exam.GetAtlas()<2 and metal >110 then return 'armatlas' end
	
	if math.random() < 0.1 and metal > 200 and #Sensors.nota_sim_exam.GetFarcks()<3 then return 'armfark' end

	if math.random() < 0.1 and metal>170 and #Sensors.nota_sim_exam.GetLuger(true)<18 then return 'armmart' end
	local attackers = #Sensors.nota_sim_exam.GetZeus(true)
	local a = 1
	if attackers < 15 then a = 1 else a = 2 end
	if attackers > 25 then a = 3 end
	if metal> 1800 then attackers =0.5 end
	
	if  metal>170 and math.random() < 0.1/ a  then return 'armzeus' end
	if metal > 401 and  math.random() < 0.1/ a then return 'armmav' end
	
	if  metal > 1500 and #Sensors.nota_sim_exam.GetLuger(true)>6 then return 'upgrade' end

end
