local sensorInfo = {
	name = "GetPoints",
	desc = "",
	author = "bentosming",
	date = "2019-04-23",
	license = "MIT"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local GetGroundHeight = Spring.GetGroundHeight
local GetTeamUnits = Spring.GetTeamUnits

-- @description 

return function(position)
	
	local listOfUnits = Spring.GetTeamUnits(0) --todo team
	local radars =  Sensors.core.FilterUnitsByCategory(listOfUnits, Categories.nota_sim_exam.seer)
	--Spring.Echo('radars' .. dump(radars))

	for i, r in ipairs(radars) do
	
	--Spring.Echo(r)
		local x,y,z = Spring.GetUnitPosition(r)

		--Spring.Echo(position and position:Distance(Vec3(x,y,z)))
		if position and position:Distance(Vec3(x,y,z)) >100 then

			return r
		end
	end
	if not position then 
		return radars
	end
	-- return
end
