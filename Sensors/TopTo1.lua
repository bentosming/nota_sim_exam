local sensorInfo = {
	name = "GetPoints",
	desc = "",
	author = "bentosming",
	date = "2019-04-23",
	license = "MIT"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching  -- !!!

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end



return function(pathId)
	if pathId == "Top" then return 1 end
	if pathId == "Middle" then return 2 end
	if pathId == "Bottom" then return 3 end
end
