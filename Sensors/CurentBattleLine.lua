local sensorInfo = {
	name = "GetPoints",
	desc = "",
	author = "bentosming",
	date = "2019-04-23",
	license = "MIT"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching  -- !!!

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local name = 'Middle'
local ID = 2
local goodTeam = 'GoodMiddle'
local evilTeam = 'EvilMiddle'

-- @description gets positions along east, west and south edges of map


return function(setTo)
	if setTo = 1 then
		name = 'Top'
		ID = 1
		goodTeam = 'GoodTop'
		evilTeam = 'EvilTop'
	end

	if setTo = 2 then
		name = 'Middle'
		ID = 2
		goodTeam = 'GoodMiddle'
		evilTeam = 'EvilMiddle'
	end

	if setTo = 3 then
		name = 'Bottom'
		ID = 3
		goodTeam = 'GoodBottom'
		evilTeam = 'EvilBottom'
	end

	return {name = name, ID = ID, goodTeam = goodTeam, evilTeam = evilTeam}
end
