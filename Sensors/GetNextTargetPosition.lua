local sensorInfo = {
	name = "GetNextTargetPosition",
	desc = "",
	author = "bentosming",
	date = "2019-04-23",
	license = "MIT"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local GetGroundHeight = Spring.GetGroundHeight
local GetTeamUnits = Spring.GetTeamUnits
local states = {}
local UnitsOnPaths = {{},{},{}}

-- @description gets positions along east, west and south edges of map

return function(unitId)
	
	if (states[unitId] == nil) then
		--potřebuje vylepšení, pokud se čeká víc jak jeden na cestu
		for _,p in ipairs(UnitsOnPaths) do
			for i, id in ipairs(p) do
				if not Spring.ValidUnitID(id) then 
					states[id]=nil 
					p[i]=nil
				end
			end
		end

		local unitOnPathsCounts = {#UnitsOnPaths[1],#UnitsOnPaths[2],#UnitsOnPaths[3]}
		local min = unitOnPathsCounts[1]
		local argmin = 1
		for id, count in ipairs(unitOnPathsCounts) do
			if min > count then 
				min = count
				argmin = id
			end
		end
		--potřebuje vylepšení, pokud se čeká víc jak jeden na cestu
		UnitsOnPaths[argmin][1]=unitId
		--urychleni
		states[unitId] = { pathId = 2, point = 1, direction = "up", next = nil} 
	end
	
	local mi = Sensors.core.MissionInfo() -- vyfiltrovat na bezpečné/zabrané, nebo d8t sensor.battleline[pathId].safePointId místo #mi
	local state = states[unitId]
	
	--local mi = Sensors.nota_sim_exam.GetPoints(state.pathId)

	if (state.point == 1 ) then state.direction = "up" end
	local max = Sensors.nota_sim_exam.LastSafeStrongPoint(state.pathId)
	if (state.point >= max ) then state.direction = "down"
	state.point = max end

	if (state.direction == "up") then state.point = state.point+1 end
	if (state.direction == "down") then state.point = state.point-1 end
	local pathName = Sensors.nota_sim_exam.OneToTop(state.pathId)
	state.next = mi.corridors[pathName].points[state.point].position
	return state
	-- return
end
