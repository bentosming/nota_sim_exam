local sensorInfo = {
	name = "GetPoints",
	desc = "",
	author = "bentosming",
	date = "2019-04-23",
	license = "MIT"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local GetGroundHeight = Spring.GetGroundHeight
local GetTeamUnits = Spring.GetTeamUnits

-- @description TODO naparsovat missioninfo tak, aby se vracela posloupnost bodu na ceste (obou typu bodu), prvni je ally base

return function(pathcount, unitId)
	local pointsCount = 10
	local maxX = Game.mapSizeX
	local maxZ = Game.mapSizeZ
	local positions = {}
	for x = 1, pointsCount do
		positions[x] = Vec3(maxX / pointsCount * x/2, 100, 600*pathcount)
	end


	return positions
	-- return
end
