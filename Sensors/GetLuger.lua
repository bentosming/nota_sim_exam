local sensorInfo = {
	name = "GetPoints",
	desc = "",
	author = "bentosming",
	date = "2019-04-23",
	license = "MIT"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local GetGroundHeight = Spring.GetGroundHeight
local GetTeamUnits = Spring.GetTeamUnits

-- @description 

return function(all)
	
	local place = Sensors.nota_sim_exam.PlaceForTower().p
	local listOfUnitsInSphere = Spring.GetUnitsInSphere(place.x, place.y, place.z, 400, 0) --todo team
	local listOfUnits = Spring.GetTeamUnits(0) --todo team
--	Spring.Echo(dump(listOfUnits))
	local farcks =  Sensors.core.FilterUnitsByCategory(listOfUnits, Categories.nota_sim_exam.luger)
--	Spring.Echo(dump(farcks))
if all then return farcks end
	for i, farck in ipairs(farcks) do
		--Spring.Echo(farck)
		local notInSphere = true
		for i, u in ipairs(listOfUnitsInSphere) do
			if u==farck then notInSphere = false end
		end
		if notInSphere then return farck end
	end

	return nil
	-- return
end
