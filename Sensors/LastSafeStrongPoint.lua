local sensorInfo = {
	name = "GetPoints",
	desc = "",
	author = "bentosming",
	date = "2019-04-23",
	license = "MIT"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching  -- !!!

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local allyTeams = Spring.GetAllyTeamList()
local paths = {	{safePointId = 1, path = Sensors.GetPoints(1)},
				{safePointId = 1, path = Sensors.GetPoints(2)},
				{safePointId = 1, path = Sensors.GetPoints(3)}}
-- @description gets positions along east, west and south edges of map

return function(pathId)
	pathId = Sensors.nota_sim_exam.OneToTop(pathId)
	local mi =  Sensors.core.MissionInfo()
	path = mi.corridors[pathId].points
	local stillSafe = 1
		for i = 1, #path do
			if path[i].isStrongpoint then
				if path[i].ownerAllyID == 0 then
					stillSafe = i
				else 
					break
				end
			end
		end
	

	return stillSafe
end
