local sensorInfo = {
	name = "GetPoints",
	desc = "",
	author = "bentosming",
	date = "2019-04-23",
	license = "MIT"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching  -- !!!

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local allyTeams = Spring.GetAllyTeamList()
local paths = {	{safePointId = 1, path = Sensors.GetPoints(1)},
				{safePointId = 1, path = Sensors.GetPoints(2)},
				{safePointId = 1, path = Sensors.GetPoints(3)}}
-- @description gets positions along east, west and south edges of map

function isOneAlliance(u)
	if #u ==0 then return true end
	local prev = Spring.GetUnitAllyTeam(u[1])
	for _, unit in ipairs(u) do
		local current = Spring.GetUnitAllyTeam(unit)
		if prev ~= current then return false end
		prev = current
	end

end

return function()

	if paths[1].info == nil then
		for  _, path in ipairs(paths) do 
			path["safe"] = {}
			for i = 1, #path.path do
				path.safe[i] = {visible =  #Spring.GetUnitsInSphere(path.path[i].x, path.path[i].y,path.path[i].z, 400)>0}
			end
		end
	end




	return paths
end
