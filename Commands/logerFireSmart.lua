function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined positions",
		parameterDefs = {
		}
	}
end

local i = 0
local x,y,z = 0,0,0
-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0
local unitTimeCounter = {}-- Vec3(0,0,0)
local unitFireAt = {}-- Vec3(0,0,0)
-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0, 0, 0)
	self.unitsInfo = {}
	self.nextPositionIndex = 0
	self.ordersGiven = false
end

function Run(self, units, parameter)
	local selectedUnits = Sensors.nota_sim_exam.GetLuger(true)
	local fight = parameter.fight -- boolean
	local position = Sensors.nota_sim_exam.PlaceForTower().dangerPoint
	



	-- pick the spring command implementing the move

	cmdID = CMD.ATTACK
	--Spring.Echo('Loger' .. #selectedUnits)
	--Spring.Echo(dump(unitTimeCounter))
--	if not self.ordersGiven then 
	local fireAt = Spring.GetUnitsInSphere(position.x, position.y, position.z, 250,5)
		for _,unit in ipairs(selectedUnits) do
			if unitTimeCounter[unit] == nil then unitTimeCounter[unit] = 0		
				SpringGiveOrderToUnit(unit, CMD.FIRE_STATE, {1}, {}) 
				return RUNNING
			 end
			
			--Spring.Echo(dump(fireAt))	
			if not Spring.ValidUnitID(unitTimeCounter[unit]) then 
				
		
				if #fireAt == 0 then 
					SpringGiveOrderToUnit(unit, cmdID, {position.x, position.y, position.z}, {}) 
					--Spring.Echo('Giving order to ' .. unit .. "position")
				else
				 	local fa = fireAt[math.random(1,#fireAt)]
					unitTimeCounter[unit] = fa
					SpringGiveOrderToUnit(unit, cmdID, {fa},{})
					--Spring.Echo('Giving order to ' .. unit .. "unit" .. fa)
				end
		--	self.ordersGiven = true
			end 
	end
		
			
	return RUNNING
end



function Reset(self)
	ClearState(self)
end
