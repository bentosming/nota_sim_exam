function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined positions",
		parameterDefs = {
			{
				name = "selectedUnit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = ""
			},
			{
				name = "fight",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false"
			},
			{
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = ""
			}
		}
	}
end

local i = 0
local x,y,z = 0,0,0
-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0
local unitPosition = {}-- Vec3(0,0,0)
-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0, 0, 0)
	self.unitsInfo = {}
	self.nextPositionIndex = 0
end

function Run(self, units, parameter)
	local selectedUnit = parameter.selectedUnit -- array of units
	local fight = parameter.fight -- boolean
	local position = parameter.position
	

	-- assign positions to units
	local unit = selectedUnit

	if unitPosition[unit] == nil then unitPosition[unit] = Vec3(0,0,0) end

	if (Spring.ValidUnitID(unit) == false) then
		return FAILURE
	end


	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
	if (fight) then
		cmdID = CMD.FIGHT
	end

	cmdID = CMD.RECLAIM
	if position ~= unitPosition[unit] then 	
		unitPosition[unit] = position
		SpringGiveOrderToUnit(unit, cmdID, {position.x, position.y, position.z, 400}, {})
		-- Spring.Echo("Reclaim order given")
	else 
		local q = Spring.GetCommandQueue(unit, -1)
		if (#q == 0) then
			-- Spring.Echo("Reclaim order finished")
			return SUCCESS
		else
			return RUNNING
		end
	end
	return RUNNING
end


function Reset(self)
	ClearState(self)
end
