function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined positions",
		parameterDefs = {
			{
				name = "positions",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = ""
			},
			{
				name = "selectedUnit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = ""
			},
			{
				name = "fight",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false"
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0, 0, 0)
	self.unitsInfo = {}
	self.nextPositionIndex = 0
end

function Run(self, units, parameter)
	local positions = parameter.positions -- array of Vec3
	local selectedUnit = parameter.selectedUnit -- array of units
	local fight = parameter.fight -- boolean

	--Spring.Echo(dump(parameter.formation))

	if (self.unitsInfo == nil) then
		self.unitsInfo = {}
		self.threshold = 0
		self.nextPositionIndex = 0
	end

	-- assign positions to units
	local unit = selectedUnit
	if self.unitsInfo[unit] == nil then
		self.unitsInfo[unit] = {treshold = 20}
		self.nextPositionIndex = (self.nextPositionIndex + 1) % #positions
	end

	if (Spring.ValidUnitID(unit) == false) then
		return FAILURE
	end

	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
	if (fight) then
		cmdID = CMD.FIGHT
	end

	local allSuccess = true

	--for i,unit in ipairs(selectedUnits) do

	local g = {x = -1000, z = -1000}
	local unitInfo = self.unitsInfo[unit]

	local closestIndex = 1

	local pointX, pointY, pointZ = SpringGetUnitPosition(unit)
	for i, targetPosition in ipairs(positions) do
		if distance(g.x, g.z, pointX, pointZ) >= distance(positions[i].x, positions[i].z, pointX, pointZ) then
			closestIndex = i
			g = targetPosition
		end
	end
	g = positions[closestIndex - 1]
	if not positions[closestIndex - 1] then
		return SUCCESS
	end

	--Spring.Echo(closestIndex+1 .." ".. g.x .." ".. g.z)
	--local targetPosition = unitInfo["targetPosition"]
	local targetPosition = Vec3(g.x, Spring.GetGroundHeight(g.x, g.z), g.z)
	local pointX, pointY, pointZ = SpringGetUnitPosition(unit)
	local unitPosition = Vec3(pointX, pointY, pointZ)
	local distance = unitPosition:Distance(targetPosition)

	--	if (distance > 40) then

	SpringGiveOrderToUnit(unit, cmdID, targetPosition:AsSpringVector(), {})
	unitInfo["lastPosition"] = unitPosition
	--	else positions[#positions] = nil end
	--end

	if (#positions == 0) then
		return SUCCESS
	else
		return RUNNING
	end
end

function distance(ax, az, bx, bz)
	return math.sqrt((ax - bx) * (ax - bx) + (az - bz) * (az - bz))
end

function Reset(self)
	ClearState(self)
end
