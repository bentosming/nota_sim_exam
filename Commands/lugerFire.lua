function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined positions",
		parameterDefs = {
		}
	}
end

local i = 0
local x,y,z = 0,0,0
-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0
local unitPosition = {}-- Vec3(0,0,0)
-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0, 0, 0)
	self.unitsInfo = {}
	self.nextPositionIndex = 0
end

function Run(self, units, parameter)
	local selectedUnits = Sensors.nota_sim_exam.GetLuger(true)
	local fight = parameter.fight -- boolean
	local position = Sensors.nota_sim_exam.PlaceForTower().dangerPoint
	



	-- pick the spring command implementing the move

	cmdID = CMD.AREA_ATTACK
	for _,unit in ipairs(selectedUnits) do
	local q = Spring.GetCommandQueue(unit, -1)
		if (#q == 0) then
		
		SpringGiveOrderToUnit(unit, CMD.FIRE_STATE, {1}, {}) 
		SpringGiveOrderToUnit(unit, cmdID, {position.x, position.y, position.z, 300}, {}) end
	end 
		
			
	return RUNNING
end


function Reset(self)
	ClearState(self)
end
